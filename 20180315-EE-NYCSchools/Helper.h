//
//  Helper.h
//  20180311-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/15/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"



@interface Helper : NSObject


/* Constants */
extern NSString * const EELastSyncDateKey;




/* Helper Methods */


/**
 <#Description#>

 @return <#return value description#>
 */
+ (AppDelegate*)appDelegate;


/**
 <#Description#>
 */
+ (void)saveContext;

+ (NSManagedObjectContext*)context;






/**
 <#Description#>

 @param aKey <#aKey description#>
 @return <#return value description#>
 */
+ (NSString *)defaultsGetStringForKey:(NSString *)aKey;


/**
 <#Description#>

 @param val <#val description#>
 @param aKey <#aKey description#>
 */
+ (void)setValue:(id)val forKey:(NSString*)aKey;

@end
