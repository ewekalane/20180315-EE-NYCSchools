//
//  AppDelegate.h
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/18/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

