//
//  EENetworkService.m
//  20180311-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/14/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import "EENetworkService.h"
#import "AFNetworking.h"
#import "Helper.h"

NSString * const SchoolListUrl     = @"https://data.cityofnewyork.us/resource/97mf-9njv.json";
NSString * const SchoolDetailsUrl  = @"https://data.cityofnewyork.us/resource/734v-jeq5.json";

@implementation EENetworkService


/**
 Description

 @param strURL desc
 @param dictParams desc
 @param success desc
 @param failure desc
 */
+ (void)requestPostUrl:(NSString *)strURL parameters:(NSDictionary *)dictParams success:(void (^)(id responce))success failure:(void (^)(NSError *error))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.operationQueue.maxConcurrentOperationCount = 1;
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager GET:strURL parameters:dictParams progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //SLocale* currentLocale = [NSLocale currentLocale];
       NSString * lastSyncDate = [[NSDate date] description];
        [Helper setValue:lastSyncDate forKey:EELastSyncDateKey];
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(error);
        }
    }];
}



/**
 Decription

 @param aResponse desc...
 @param aClassName desc..
 @return desc...
 */
+ (id)serializeResponse:(id)aResponse ofClassName:(NSString*)aClassName{
    id obj = [[NSClassFromString(aClassName) alloc] init];
    if([aResponse isKindOfClass:[obj class]]) {
        return aResponse;
    }
    else {
        id obj = [[NSClassFromString(aClassName) alloc] init];
        obj = [NSJSONSerialization JSONObjectWithData:aResponse options:NSJSONReadingAllowFragments error:nil];
        return obj;
    }
}



/**
 Description
 
 @param success sucess -
 @param failure failure -
 */
+ (void)requestSchoolsListSuccess:(void (^)(NSArray *responce))success failure:(void (^)(NSError *error))failure {
    [EENetworkService requestPostUrl:SchoolListUrl parameters:nil success:^(id response) {
        NSArray* resp  = [self serializeResponse:response ofClassName:NSStringFromClass([NSArray class])];
        success(resp);
    } failure:^(NSError *error) {
        failure(error);
    }];
}


/**
 Description

 @param success sucess -
 @param failure failure -
 */
+ (void)requestSchoolsDetailsSuccess:(void (^)(NSArray *responce))success failure:(void (^)(NSError *error))failure {
    [EENetworkService requestPostUrl:SchoolDetailsUrl parameters:nil success:^(id response) {
        NSArray* resp  = [self serializeResponse:response ofClassName:NSStringFromClass([NSArray class])];
        success(resp);
    } failure:^(NSError *error) {
        failure(error);
    }];
}











@end
