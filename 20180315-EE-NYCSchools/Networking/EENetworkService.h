//
//  EENetworkService.h
//  20180311-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/14/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "_0180315_EE_NYCSchools+CoreDataModel.h"

@interface EENetworkService : NSObject


/**
 Get Request.

 @param strURL - URL for request
 @param dictParams - parameters for request
 @param success - success block returns an id
 @param failure - fail block returns NSError
 */
+ (void)requestPostUrl:(NSString *)strURL parameters:(NSDictionary *)dictParams success:(void (^)(id responce))success failure:(void (^)(NSError *error))failure;


/**
 <#Description#>

 @param success <#success description#>
 @param failure <#failure description#>
 */
+ (void)requestSchoolsListSuccess:(void (^)(NSArray *responce))success failure:(void (^)(NSError *error))failure;

/**
 <#Description#>

 @param success <#success description#>
 @param failure <#failure description#>
 */
+ (void)requestSchoolsDetailsSuccess:(void (^)(NSArray *responce))success failure:(void (^)(NSError *error))failure;



@end
