//
//  MasterViewController.m
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/18/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "EENetworkService.h"
#import "Helper.h"
#import "SyncHelper.h"
#import "SVProgressHUD.h"

NSString * const ViewTitle = @"2017 DOE High School Directory";

NSString * const SchoolNameKey = @"school_name";
NSString * const SchoolCityKey = @"city";
NSString * const SchoolDBNKey  = @"dbn";

@interface MasterViewController ()

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = ViewTitle;
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Refresh"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(loadSchoolList)];
    [self.navigationItem setRightBarButtonItem:item animated:YES];
    if (![[SyncHelper sharedManager] isSchoolLoaded]) {
         [self loadSchoolList];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)loadSchoolList{
    [SVProgressHUD show];
    __weak __typeof(self) weakSelf = self;
    [EENetworkService requestSchoolsListSuccess:^(NSArray *response) {
        [weakSelf insertSchoolListToStore:response];
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:@"Oops. An error occured during fetch."];
        });
    }];
}


#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        School *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        DetailViewController *controller = (DetailViewController *)[segue destinationViewController];
        [controller setDetailItem:object];
    }
}


#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    School *school = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self configureCell:cell withSchool:school];
    return cell;
}


- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title =  [Helper defaultsGetStringForKey:EELastSyncDateKey] ? : @"Never";
    return [NSString stringWithFormat:@"Last Updated: %@", title];
}


- (void)configureCell:(UITableViewCell *)cell withSchool:(School *)school {
    cell.textLabel.text         = school.name;
    cell.detailTextLabel.text   = school.borough;
}


#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}



#pragma mark - Fetched results controller
- (void)insertSchoolListToStore:(NSArray*)schools {
    BOOL resetOk=[Helper deleteAllObjects:@[@"School"] inContext:[Helper context]];
    if (resetOk){
        NSManagedObjectContext * MOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        MOC.persistentStoreCoordinator  = [Helper context].persistentStoreCoordinator;
        [MOC performBlockAndWait:^{
            for (NSDictionary* school  in schools) {
                School *s = [[School alloc]initWithContext:[Helper  context]];
                s.name    = school[SchoolNameKey];
                s.borough = school[SchoolCityKey];
                s.dbn     = school[SchoolDBNKey];
            }
        }];
        
        NSError *error = nil;
        if (![[Helper context] save:&error]) {
            abort();
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SyncHelper sharedManager] flagSchoolsLoaded];
            [SVProgressHUD dismiss];
        });
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:@"Unable to refresh."];
        });
    }
    
    
}

- (NSFetchedResultsController<School *> *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest<School *> *fetchRequest = School.fetchRequest;
    [fetchRequest setFetchBatchSize:20];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSFetchedResultsController<School *> *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[Helper context] sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
}


@end

