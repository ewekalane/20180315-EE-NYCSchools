//
//  DetailViewController.h
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/18/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "_0180315_EE_NYCSchools+CoreDataModel.h"
#import "Helper.h"
#import "EENetworkService.h"


@interface DetailViewController : UIViewController

@property (strong, nonatomic) School *detailItem;
@property (weak, nonatomic) IBOutlet UITextView *detailDescriptionLabel;

@end

