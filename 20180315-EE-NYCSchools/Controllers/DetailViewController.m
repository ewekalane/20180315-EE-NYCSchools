//
//  DetailViewController.m
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/18/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import "DetailViewController.h"
#import "SVProgressHUD.h"
#import "SyncHelper.h"

NSString * const SchoolAvgMathScoreKey      = @"sat_math_avg_score";
NSString * const SchoolAvgReadingScoreKey   = @"sat_critical_reading_avg_score";
NSString * const SchoolAvgWritingScoreKey   = @"sat_writing_avg_score";
NSString * const SchoolNumberOfTestTakerKey = @"num_of_sat_test_takers";
NSString * const SSchoolDBNKey              = @"dbn";


@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Refresh"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(loadSchoolList)];
    [self.navigationItem setRightBarButtonItem:item animated:YES];
    if ([[SyncHelper sharedManager] loadedDetails]) {
        [self configureView];
    }
    else{
        [self loadSchoolList];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView {
    [SVProgressHUD show];
    if (self.detailItem) {
        self.title    = self.detailItem.name;
        SATDetails* s = [self getSchoolDetailsWithDBN:self.detailItem.dbn];
        if (s) {
            self.detailDescriptionLabel.text = [NSString stringWithFormat:@"No. of test takers - %@ \nAverage Math. Score - %@ \nAverage Reading Score - %@ \nAverage Writing Score - %@", s.number_of_takers , s.avg_math_score, s.avg_reading_score,s.avg_writing_score ];
        }
        else{
            self.detailDescriptionLabel.text = @"! Error occured. Unable to load details.";
        }
    }
    [SVProgressHUD dismiss];
}


-(void)loadSchoolList{
    __weak __typeof(self) weakSelf = self;
    [EENetworkService requestSchoolsDetailsSuccess:^(NSArray *response) {
        [weakSelf insertSchoolListToStore:response];
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:@"Oops. An error occured during fetch."];
        });
    }];
}


- (void)insertSchoolListToStore:(NSArray*)satDetails {
    __weak __typeof(self) weakSelf = self;
    BOOL resetOk=[Helper deleteAllObjects:@[@"SATDetails"] inContext:[Helper context]];
    if (resetOk){
        NSManagedObjectContext * MOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        MOC.persistentStoreCoordinator  = [Helper context].persistentStoreCoordinator;
        [MOC performBlockAndWait:^{
            for (NSDictionary* detail  in satDetails) {
                SATDetails * s        = [[SATDetails alloc]initWithContext:[Helper context]];
                s.dbn                 = (NSString*)detail[SSchoolDBNKey];
                s.avg_math_score      = (NSString*)detail[SchoolAvgMathScoreKey];
                s.avg_reading_score   = (NSString*)detail[SchoolAvgReadingScoreKey];
                s.avg_writing_score   = (NSString*)detail[SchoolAvgWritingScoreKey];
                s.number_of_takers    = (NSString*)detail[SchoolNumberOfTestTakerKey];
            }
        }];
        
        NSError *error = nil;
        if (![[Helper context] save:&error]) {
            abort();
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [weakSelf configureView];
        });
        
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:@"Oops. An error occured during fetch."];
        });
    }
}

- (SATDetails*)getSchoolDetailsWithDBN:(NSString*)dbn{
    NSManagedObjectContext* context = [Helper context];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SATDetails" inManagedObjectContext:context];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dbn = %@",dbn];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *model=[context executeFetchRequest:fetchRequest error:&error];
    if([model count]==0)return nil;
    else return [model objectAtIndex:0];
}


#pragma mark - Managing the detail item
- (void)setDetailItem:(School *)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
    }
}


@end


