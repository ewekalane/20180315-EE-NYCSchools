//
//  MasterViewController.h
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/18/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "_0180315_EE_NYCSchools+CoreDataModel.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) NSFetchedResultsController<School *> *fetchedResultsController;



@end

