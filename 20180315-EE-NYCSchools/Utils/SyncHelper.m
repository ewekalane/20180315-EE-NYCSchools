//
//  SyncHelper.m
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/19/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import "SyncHelper.h"

@implementation SyncHelper

+ (instancetype)sharedManager {
    static SyncHelper *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[SyncHelper alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        self.loadedSchools = FALSE;
        self.loadedDetails = FALSE;
    }
    return self;
}

- (void)resetFlags{
    self.loadedSchools = FALSE;
    self.loadedDetails = FALSE;
}

- (BOOL)isSchoolLoaded{
    return self.loadedSchools;
}

- (BOOL)isDetailsLoaded{
    return self.loadedDetails;
}
- (void)flagSchoolsLoaded{
    self.loadedSchools = TRUE;
}

- (void)flagDetailsLoaded{
    self.loadedDetails = TRUE;
}
@end
