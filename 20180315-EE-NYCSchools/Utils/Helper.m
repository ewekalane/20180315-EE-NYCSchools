//
//  Helper.m
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/16/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import "Helper.h"


@implementation Helper

/* Constants */
NSString * const EELastSyncDateKey = @"EE_last_sync_date";


+ (AppDelegate*)appDelegate{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

+ (void)saveContext{
    [[Helper appDelegate] saveContext];
}

+ (NSManagedObjectContext*)context{
    return [[[Helper appDelegate] persistentContainer] viewContext];
}


+ (NSString *)defaultsGetStringForKey:(NSString *)aKey{
    NSString * rv =[[NSUserDefaults standardUserDefaults] objectForKey:aKey];
    return rv;
}

+ (void)setValue:(id)val forKey:(NSString*)aKey{
    [[NSUserDefaults standardUserDefaults] setValue:val forKey:aKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (BOOL)deleteAllObjects:(NSArray *)entityDescriptionArray inContext:(NSManagedObjectContext*)context{
    BOOL resetOK = YES;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError *error;
    
    for (NSString* entityDescription in entityDescriptionArray) {
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        for (NSManagedObject *managedObject in items) {
            [context deleteObject:managedObject];
        }
    }
    if (![context save:&error]) {
        resetOK = NO;
    }
    return resetOK;
}


@end
