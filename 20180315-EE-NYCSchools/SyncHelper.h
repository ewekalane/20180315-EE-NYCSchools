//
//  SyncHelper.h
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/19/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncHelper : NSObject

+ (id)sharedManager;

- (void)resetFlags;
- (BOOL)isSchoolLoaded;
- (BOOL)isDetailsLoaded;
- (void)flagSchoolsLoaded;
- (void)flagDetailsLoaded;

@property (assign,nonatomic) BOOL loadedSchools;
@property (assign,nonatomic) BOOL loadedDetails;

@end
