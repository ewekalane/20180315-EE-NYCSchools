//
//  main.m
//  20180315-EE-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 3/18/18.
//  Copyright © 2018 Coding Baba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
